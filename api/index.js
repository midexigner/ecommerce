const express = require("express");
const app = express();
const mongoose = require("mongoose");
const dotenv  = require('dotenv')
const userRoute = require('./routes/user');
const authRoute = require('./routes/auth');

dotenv.config();
const port = process.env.PORT || 5000;

mongoose.connect(process.env.MONGO_URL)
.then(()=>console.log("DBConnection Successfull!"))
.catch((err)=>console.log(err));

app.use(express.json());
app.use('/api/auth/',authRoute);
app.use('/api/user/',userRoute);

app.get('/api/test',(req,res)=>{
console.log("test is successfull");
});

app.listen(port,()=>{
    console.info("Your Port is " +port);
    console.log("Backend server is running!");
});

/* 
https://www.youtube.com/watch?v=h0gWfVCSGQQ&ab_channel=edureka%21
https://www.youtube.com/watch?v=b7GC4Zr74M0&ab_channel=Intellipaat
https://www.youtube.com/watch?v=UrwbeOIlc68&ab_channel=edureka%21
https://www.youtube.com/channel/UCq3erTHXOjr45nHtTFqRDuA/playlists

*/