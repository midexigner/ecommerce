const express = require("express");
const router = express.Router();
const  CryptoJS = require("crypto-js");
const User = require('../models/User');


// Register
router.post("/register",async (req,res)=>{
    const newUser = new User({
        username:req.body.username,
        email:req.body.email,
        password:CryptoJS.AES.encrypt(req.body.password,process.env.PASS_SEC).toString()
    });

    try{
        const savedUser = await newUser.save();
        res.status(201).json(savedUser);
    }catch(err){
        console.log(err);
        res.status(403).json(err);
    }

});

// Login
router.post("/login",async (req,res)=>{

    try{
        const user = await User.findOne( { username: req.body.username } )
    }
    catch(err){
        console.log(err);
    }

});


module.exports = router;